# Audsat Devops Files
Esse repositório tem como objetivo gerenciar/versionar arquivos utilizados nas aplicações voltadas a todo ecossistema de serviços e ferramentas na área de DevOps/Tecnologia. Estão disponíveis, por exemplo, arquivos que definirão quais ambientes serão ligados, em que hora e em que dia.

## Pré-requisitos

* Estruturação de Json
* Crontab
* Git

## Redeploys
A funcionalidade de *redeploy*, tem como objetivo definir quais aplicações, de quais ambientes e em que horário irão iniciar o processo de redeploy. 

### Como utilizar?
Será necessário editar o arquivo *workloads.json* que se encontra dentro da pasta redeploy.

#### Estrutura

A estrutura do arquivo se baseia em uma lista de *clusters* onde temos:
- *namespace*
- Lista de *workloads* com tipo e nome
- *cronTime*

**O *cronTime* deve ser escrito no padrão *Cron*.**

Veja a seguir, um exemplo, onde está sendo feito o processo com o módulo do geo-credito no ambiente de prod-santander (todos os dias às 00h00): 

```json
{
    "cronTime": "0 0 * * *",
    "namespace": "prod-santander",
    "workloads": [{
        "name": "audsatplatform-geo-credito-agricola",
        "kind": "Deployment"
    }],
    "kubeConfig": "kubeOracleLegadoProd"
}
```

### Informações importantes

Após a alteração, será necessário fazer a 'implantação' da sua alteração. Para isso, vamos extrair o hash do seu *commit*. 
Caso você não saiba, basta executar o seguinte comando no seu terminal, onde o valor vem logo em seguida da parala *commit*.

```bash
git log
```

Com o código em mãos, deve-se ir até à aplicação ***audsatplatform-redeploy-crons*** que se encontra no *namespace* ***audsat-tools***, em editar e modificar a variável de ambiente **AUDSATPLATFORM_REDEPLOY_CRONS_S3_WORKLOADS_URL**. O hash do commit deve ser substituido como no seguinte exemplo: raw/*HASH_COMMIT*/redeploys/. 

Basta salvar, e sua alteração será implementada.
	

## Life Times
A funcionalidade de *lifetime*, tem como objetivo definir quais ambientes, em que horário e dia irão ligar e desligar.

### Como utilizar?
Será necessário editar o arquivo *namespaces.json* que se encontra dentro da pasta redeploy.

#### Estrutura

A estrutura do arquivo se baseia em uma lista de *namespace* onde temos:
- *nome*
- *cluster*, nesse parâmetro temos duas opções prod/dev. Todo ambiente que não é PROD, deve-se colocar o valor *dev*.
- *nodePoolId*, endereço do node pool no serviço de cloud
- *cloud*
- *kubeConfig*, o kubeConfig pertencente ao o cluster em questão
- *cronTimeStart*, o dia e hora que o ambiente deve ser ligado
- *cronTimeStop*, o dia e hora que o ambiente deve ser desligado
- *crontimeStartActive*, esse parâmetro é booleano, sendo responsável por definir se o crontime de inicialização deve ser respeitado
- *crontimeStopActive*, esse parâmetro é booleano, sendo responsável por definir se o crontime de encerramento deve ser respeitado


Veja a seguir, um exemplo, onde está sendo feito o processo com o *namespace* de QA iniciando todos os dias úteis às 8h00 e encerrando às 19h00:

```json
{
    "nome": "qa-audsat",
    "cluster": "dev",
    "rds_id": "",
    "ec2_id": "",
    "status": "",
    "nodePoolId": "ocid1.nodepool.oc1.sa-saopaulo-1.aaaaaaaaaeztcojwhbsdmmbtmjstkztggq4ggojtgjtgmzlggnstimbqmztg",
    "cloud": "oracle",
    "kubeConfig": "kubeOracleAudsatDev",
    "deployments": [],
    "cronTimeStart": "0 8 * * 1-5",
    "cronTimeStop": "0 19 * * 1-5",
    "crontimeStartActive": false,
    "crontimeStopActive": false
}
```

**Observação: Os demais campos podem estar vazios**

### Informações importantes

Após a alteração, será necessário fazer a 'implantação' da sua alteração. Para isso, vamos extrair o hash do seu *commit*. 
Caso você não saiba, basta executar o seguinte comando no seu terminal, onde o valor vem logo em seguida da parala *commit*.

```bash
git log
```

Com o código em mãos, deve-se ir até à aplicação ***audsatplatform-namespace-lifetime-crons*** que se encontra no *namespace* ***audsat-tools***, em editar e modificar a variável de ambiente **AUDSATPLATFORM_NAMESPACE_LIFETIME_CRONS_S3_NAMESPACES_URL**. O hash do commit deve ser substituido como no seguinte exemplo: raw/*HASH_COMMIT*/lifetime/. 

Basta salvar, e sua alteração será implementada.

## Materiais de apoio

[Crontab Generator](https://crontab-generator.org/)